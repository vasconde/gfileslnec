# des:    script para gerar o ficheiro de equações para os progrmas
#         antigos de ajustamento do LNEC
# author: vasconde
# versao: v1 12/12/6

# --- Dados --- # --- Pode ser editado --- #

# nome das marcas (1 por linha)
nome_in_file_1 = "coo.txt"

# pares de marcas (1 par por linha e os seus elementos separados por virgula)
nome_in_file_2 = "eq_nomes.txt" 

# ficheiro de saida
nome_out_file = "eq.txt"

# --- ----- --- #

# --- Prog- --- # --- Não pode ser editado* --- #

# recolhe o nome das marcas
in_file_ref = open(nome_in_file_1, "rt")
names = [line.strip() for line in in_file_ref]
in_file_ref.close()
#print (names)

# recolhe as linhas do ficheiro (pares atras,frente)
in_file = open(nome_in_file_2, "rt")
pares_str = [line.strip() for line in in_file] #lista de pares em string
in_file.close()
#print (pares_str)

# passa as linhas para listas de pares [atras,frente]
pares = [par.rsplit(',') for par in pares_str] #lista de pares (em listas)
#print(pares)

# passa os pares para numeros segundo
# a ordem da lista de nomes
pares_id = [[names.index(par[0])+1, names.index(par[1])+1] for par in pares]
print(pares_id)

# escreve no ficheiro
out_file = open(nome_out_file, "wt")
for par_id in pares_id:
    out_file.write(str(par_id[0]))
    out_file.write(',')
    out_file.write(str(par_id[1]))
    out_file.write('\n')
out_file.close()
